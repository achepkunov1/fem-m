#include "Solve.h"

#include "Mesh.h"


namespace Solver {

    /*!
     * @param normals
     * @return 6 vectors - cross between faces 1-2, 1-3, 1-4, 2-3, 2-4, 3-4
     */
    Eigen::Matrix<double, 3, 6> crossOfNormals(const Eigen::Matrix<double, 3, 4>& normals) {
        Eigen::Matrix<double, 3, 6> rotPhi;
        size_t i = 0;
        for(size_t a = 0; a < 3; a++) {
            for(size_t b = a+1; b < 4; b++) {
                //rotPhi(:,:,1) = 2*mycross(Dlambda(:,:,1),Dlambda(:,:,2),2);
                rotPhi.col(i) = 2*normals.col(a).cross(normals.col(b));
                i++;
            }
        }
        return rotPhi;
    }

    /*!
     * return
     * @param normals
     * @return 4x4 matrix of scalars - (i,j) cell contains normals(i)*normals(j)
     */
    Eigen::Matrix4d dotOfNormals(const Eigen::Matrix<double, 3, 4>& normals) {
        Eigen::Matrix4d DiDj;
        for(size_t a = 0; a < 4; a++) {
            for(size_t b = a+1; b < 4; b++) {
                //DiDj(:,i,j) = dot(Dlambda(:,:,i),Dlambda(:,:,j),2);
                DiDj(a, b) = normals.col(a).dot(normals.col(b));
                DiDj(b, a) = DiDj(a, b);
            }
        }
        return DiDj;
    }

    //       % mass matrix M
    //      i1 = locEdge(i,1); i2 = locEdge(i,2);
    //      j1 = locEdge(j,1); j2 = locEdge(j,2);
    //
    //      % (phi_i,phi_j)
    //      sMphi(index+1:index+NT) = (1/20)*sig(sdl).*volume.*...
    //         ((1+(i1==j1))*DiDj(:,i2,j2) ...
    //         -(1+(i1==j2))*DiDj(:,i2,j1) ...
    //         -(1+(i2==j1))*DiDj(:,i1,j2) ...
    //         +(1+(i2==j2))*DiDj(:,i1,j1));
    //      % (psi_i,psi_j)
    //      sMpsi(index+1:index+NT) = (1/20)*volume.*sig(sdl).*...
    //         ((1+(i1==j1))*DiDj(:,i2,j2) ...
    //         +(1+(i1==j2))*DiDj(:,i2,j1) ...
    //         +(1+(i2==j1))*DiDj(:,i1,j2) ...
    //         +(1+(i2==j2))*DiDj(:,i1,j1));
    // i1 is first node of first edge in tetrahedron
    // i2 is second node of first edge in tetrahedron
    // j1 is first node of second edge in tetrahedron
    // j2 is second node of second edge in tetrahedron
    // for phi-phi: +--+
    // for psi-psi: ++++
    // for phi-psi: +-+-
    static uint8_t locEdge1[] = {0, 0, 0, 1, 1, 2};
    static uint8_t locEdge2[] = {1, 2, 3, 2, 3, 3};

    double massElemPhiPhi(const Eigen::Matrix4d& D, size_t i, size_t j) {
        size_t i1 = locEdge1[i];
        size_t i2 = locEdge2[i];
        size_t j1 = locEdge1[j];
        size_t j2 = locEdge2[j];
        double res =
           (1+int(i1==j1))*D(i2,j2)
          -(1+int(i1==j2))*D(i2,j1)
          -(1+int(i2==j1))*D(i1,j2)
          +(1+int(i2==j2))*D(i1,j1);

        return res;
    }
    double massElemPsiPsi(const Eigen::Matrix4d& D, size_t i, size_t j) {
        size_t i1 = locEdge1[i];
        size_t i2 = locEdge2[i];
        size_t j1 = locEdge1[j];
        size_t j2 = locEdge2[j];
        double res =
                (1+int(i1==j1))*D(i2,j2)
               +(1+int(i1==j2))*D(i2,j1)
               +(1+int(i2==j1))*D(i1,j2)
               +(1+int(i2==j2))*D(i1,j1);

        return res;
    }

    double massElemPsiPhi(const Eigen::Matrix4d& D, size_t i, size_t j) {
        size_t i1 = locEdge1[i];
        size_t i2 = locEdge2[i];
        size_t j1 = locEdge1[j];
        size_t j2 = locEdge2[j];
        double res =
                (1+int(i1==j1))*D(i2,j2)
                -(1+int(i1==j2))*D(i2,j1)
                +(1+int(i2==j1))*D(i1,j2)
                -(1+int(i2==j2))*D(i1,j1);

        return res;
    }

/*!
 *
 * @param mesh
 * @param xis
 * @param sigmas
 * @param pA
 * @param pM
 * @param makeHalfMatrices - A & M is symmetric, so we can create only lower-left part of matrices (L) with half values in main diagonal, so L+L^t = M
 * @return
 */
    bool makeMatrices(
            const Mesh& mesh,
            const std::vector<double>& xis,
            const std::vector<double>& sigmas,
            Eigen::SparseMatrix<double>* pA,
            Eigen::SparseMatrix<double>* pM,
            bool makeHalfMatrices) {

        // EddyCurr3N1 with typeRHS = rotF

        // pA - pointer to stiffness matrix.
        // pM - pointer to mass matrix.
        // size of matrix is 2*NE (NE=count of edges)
        // A(1..NE, 1..NE) is stiffness matrix
        // M(1..NE, 1..NE) is mass matrix for (phi_i,phi_j)
        // M(NE+1..2*NE, NE+1..2*NE) is mass matrix for (psi_i,psi_j)
        // M(1..NE, NE+1..2*NE) is mass matrix for (phi_i,psi_j)
        // M(NE+1..2*NE, 1..NE) is also mass matrix for (phi_i,psi_j)
        // A = ( A  0 )
        //     ( 0  0 )
        //
        // M = ( HH HS )
        //     ( HS SS )
        //

        // locEdge = [1 2; 1 3; 1 4; 2 3; 2 4; 3 4];
        //Ndof = 2*NE
        size_t NE = mesh.edges.rows();

        size_t tripletsACount = mesh.tetrahedrons.size() * ((makeHalfMatrices)?21:36); // 21=6+5+...+1
        size_t tripletsMCount = tripletsACount*2 + mesh.tetrahedrons.size() * 36 * ((makeHalfMatrices)?1:2);
        typedef Eigen::Triplet<double> Triplet;
        std::vector<Triplet> aTriplets;  aTriplets.reserve(tripletsACount);
        std::vector<Triplet> mTriplets;  mTriplets.reserve(tripletsMCount);

        for(size_t thI = 0; thI < mesh.tetrahedrons.size(); thI++ ) {

            // prepare geometry-related values for tetrahedron
            double volume = mesh.thVolume(thI);
            Eigen::Matrix<double, 3, 4> normals = mesh.thNormals(thI);

            double kv = 1.0/(6.0*volume);
            double kv2 = kv*kv;

            Eigen::Matrix<double, 3, 6> rotPhi = crossOfNormals(normals)*kv2;

            Eigen::Matrix4d DiDjV = dotOfNormals(normals)*kv2*volume/20; // = DiDj * volume/20

            // add 6*(6+1)/2 rows to i-j-value-triplets and/or 6*6 triplets
            // according to makeHalfMatrices

            Mesh::IndexSdl iSdl = mesh.sdl[thI];

            for(size_t i = 0; i < 6; i++) {
                for(size_t j = i; j < 6; j++) {
                    size_t ii = mesh.tetrahedron2Edges(thI, i);
                    size_t jj = mesh.tetrahedron2Edges(thI, j);

                    double vA = xis[iSdl] * volume * rotPhi.col(i).dot(rotPhi.col(j));
                    double vMPhi = massElemPhiPhi(DiDjV, i, j);
                    double vMPsi = massElemPsiPsi(DiDjV, i, j);

                    if(makeHalfMatrices) {
                       if (ii==jj) {
                           vA /= 2;
                           vMPhi /= 2;
                           vMPsi /= 2;
                       }
                    } else {
                        if (ii!=jj) {
                            aTriplets.push_back(Triplet(jj, ii, vA));
                            mTriplets.push_back(Triplet(jj, ii, vMPhi));
                            mTriplets.push_back(Triplet(jj+NE, ii+NE, vMPhi));
                        }
                    }
                    aTriplets.push_back(Triplet(ii, jj, vA));
                    mTriplets.push_back(Triplet(ii, jj, vMPhi));
                    mTriplets.push_back(Triplet(ii+NE, jj+NE, vMPhi));

                }
            }


        }// for(size_t thI = 0; thI < mesh.tetrahedrons.size(); thI++ )
        return false;
    }

    void prepareCoefficientsOfMedium(const Parameters& parameters, std::vector<double>* pXis, std::vector<double>* pSigmas) {
        assert(parameters.sigmas.size() == parameters.mus.size());
        std::vector<double>& xis = *pXis;
        std::vector<double>& sigmas = *pSigmas;
        xis.resize(parameters.sigmas.size());
        sigmas.resize(parameters.sigmas.size());

        const double C = 299792458;
        const double cgc_sig = 8987550000;
        const double pi = double(3.141592653589793238462643383279502884L);
        for(size_t i = 0; i < parameters.sigmas.size(); i++){
            double s = parameters.sigmas[i];
            if (s == 0.0)
                s = parameters.sigma0;
            xis[i] = 1/(4*pi*parameters.mus[i]);
            sigmas[i] = cgc_sig*parameters.sigmas[i]/(C*C);
        }

    }

    Result go(const Parameters& parameters, const Mesh& mesh) {
        Result res;

        if(mesh.tetrahedrons.rows() == 0)
            return res;

        //assert(mesh.sdl.minCoeff() == 1);
        //assert(mesh.sdl.maxCoeff() == parameters.mus.size());

        Eigen::SparseMatrix<double> A;
        Eigen::SparseMatrix<double> M;

        std::vector<double> xis, sigmas;
        prepareCoefficientsOfMedium(parameters, &xis, &sigmas);
        makeMatrices(mesh, xis, sigmas, &A, &M );


        return res;
    }

}
