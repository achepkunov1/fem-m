#pragma once

#include "Mesh.h"
#include "Parameters.h"
#include "Result.h"

#include <Eigen/Sparse>

namespace Solver {

    bool makeMatrices(
            const Mesh& mesh,
            const std::vector<double>& xis,
            const std::vector<double>& sigmas,
            Eigen::SparseMatrix<double>* pA,
            Eigen::SparseMatrix<double>* pM,
            bool makeHalfMatrices = false );

    Result go(const Parameters& parameters, const Mesh& mesh);

}

