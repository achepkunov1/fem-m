#include "generateMesh.h"

#include <tclap/CmdLine.h>

#include <fstream>

int main(int argc, char** argv)
{
    // Wrap everything in a try block.  Do this every time,
    // because exceptions will be thrown for problems.
    try {

        TCLAP::CmdLine cmd("generate quality field of mesh", ' ', "0.1.0");

        // reverse order of args

        TCLAP::SwitchArg silent("s","silent","not show summary of file", cmd, false);


        TCLAP::ValueArg<std::string> inputPath("j","job","Json with description of unv-file, important sizes etc",true,"","path", cmd);

        // Parse the argv array.
        cmd.parse( argc, argv );

        // Do what you intend.

        return generateMesh(inputPath.getValue(), silent.getValue());

//            std::string jsonSrc = testDataDir + "/ms-mesh-yz-2t-sss-110k-coreAsCoil.json";


    } catch (TCLAP::ArgException &e)  // catch any exceptions
    {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        return -1;
    }
    catch(std::runtime_error& e) {
        std::cerr << "runtime error: " << e.what() << std::endl;
        return 3;
    }
}