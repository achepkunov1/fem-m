#include "generateMesh.h"

#include "io/DB.h"


int generateMesh(std::string jsonSrc, bool silent) {
    DB db("");
    Parameters p;
    p.kUnit = 1;
    //p.sourceFile = testDataDir + "/store-test-2.unv";
    //p.resultFile = testDataDir + "/store-test-2.mat";

    size_t count = 50;
    double R = 50.0;
    double h = 5.0;
    double z = 0.0;

    Mesh mesh;
    mesh.nodes.resize(count*3, 3);

    Mesh::Index base1 = 0;
    Mesh::Index base2 = count;
    Mesh::Index base3 = count*2;
    static double piK = std::acos(-1.0)/count/2; // k = 4*i
    for(size_t i = 0; i < count; i++ ) {
        double a = 4*i*piK;
        double x = R*sin(a);
        double y = R*cos(a);
        mesh.nodes.row(i+base1) = Eigen::RowVector3f(x, y, z);
    }
    for(size_t i = 0; i < count; i++ ) {
        double a = (4*i+2)*piK;
        double x = R*sin(a);
        double y = R*cos(a);
        mesh.nodes.row(i+base2) = Eigen::RowVector3f(x, y, z+h);
        a = (4*i+3)*piK;
        x = (R-h)*sin(a);
        y = (R-h)*cos(a);
        mesh.nodes.row(i+base3) = Eigen::RowVector3f(x, y, z+h/2);
    }

    mesh.tetrahedrons.resize(count*3, 4);
    for(Mesh::Index i = 0; i < count; i++ ) {
        Mesh::Index i1 = (i+1)%count;
        mesh.tetrahedrons.row(3*i)   = Eigen::RowVector4<Mesh::Index>{i+base1, i1+base1, i+base2, i+base3};
        mesh.tetrahedrons.row(3*i+1) = Eigen::RowVector4<Mesh::Index>{i1+base1, i1+base2, i+base2, i+base3};
        mesh.tetrahedrons.row(3*i+2) = Eigen::RowVector4<Mesh::Index>{i1+base1, i1+base3, i1+base2, i+base3};
    }

    bool storeRes = db.save(p, mesh, jsonSrc);

    if(storeRes)
        return 0;

    return 255;

}
