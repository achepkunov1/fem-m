#pragma once

#include <string>

struct MapParameters {
    int maxXTicks = 0;
    int maxYTicks = 0;
    bool logScale = false;
    double minX   = 0;
    double minY   = 0;
};

int qualityCheck(std::string jsonSrc, std::ostream* pOutputFile, const MapParameters& parameters, bool silent);
