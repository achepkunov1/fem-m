#include "qualityChecker.h"

#include <common/NiceScale.h>
#include <datatypes/Mesh.h>
#include <datatypes/Parameters.h>
#include <io/DB.h>
#include <mesh/MeshUtils.h>

#include <iostream>
#include <iomanip>

static std::ostream& printInt(std::ostream& stream)
{
    stream.setf(std::ios::right, std::ios::adjustfield);
    //stream.setf(std::ios::scientific, std::ios::floatfield);
    //stream.precision(16);
    stream.width(8);
    return stream;
}

int qualityCheck(std::string jsonSrc, std::ostream* pOutputFile, const MapParameters& mapParameters, bool silent) { // int maxX, int maxY, bool logScale,
    std::ostream& outputFile =  *pOutputFile;

    DB db("");

    Parameters parameters;

    double kxy = 1.0/parameters.kUnit;

    Mesh mesh;
    bool loadRes = db.load(jsonSrc, &parameters, &mesh);

    if(!loadRes)
        throw std::runtime_error("source file not loaded");

    Eigen::Matrix<double, Eigen::Dynamic, 5> field = MeshUtils::getRZQualityField(mesh);

    //auto lnz = field.col(0).unaryExpr([kxy](double x){return std::log10(x*kxy);});
    //auto lnr = field.col(1).unaryExpr([kxy](double x){return std::log10(x*kxy);});
    auto lnz = field.col(0);
    auto lnr = field.col(1);

    double minR =  std::max(lnr.minCoeff(), mapParameters.minX);
    double minZ =  std::max(lnz.minCoeff(), mapParameters.minY);

    if(mapParameters.logScale) {
        minR = std::log10(minR);
        minZ = std::log10(minZ);
        for(auto& x : lnz)
          x = std::log10(x);
        for(auto& x : lnr)
          x = std::log10(x);
    }

    NiceScale rScale(minR, lnr.maxCoeff(), mapParameters.maxXTicks);
    NiceScale zScale(minZ, lnz.maxCoeff(), mapParameters.maxYTicks);

    Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic> fmMin(zScale.ticks, rScale.ticks); // field matrix for mu
    Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic> fmSum(zScale.ticks, rScale.ticks); // field matrix
    Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic> ftMin(zScale.ticks, rScale.ticks); // field matrix for tau
    Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic> ftSum(zScale.ticks, rScale.ticks); // field matrix
    Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic> fvMin(zScale.ticks, rScale.ticks); // field matrix for volume
    Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic> fvMax(zScale.ticks, rScale.ticks); // field matrix for volume
    Eigen::Array<double, Eigen::Dynamic, Eigen::Dynamic> fvSum(zScale.ticks, rScale.ticks); // field matrix
    Eigen::Array<int, Eigen::Dynamic, Eigen::Dynamic> fmC(zScale.ticks, rScale.ticks);

    double muMin = 2;
    double tauMin = 2;
    double vMin = std::numeric_limits<double>::max();
    double vMax = -std::numeric_limits<double>::max();
    fmSum.fill(0);
    fmMin.fill(2);
    ftSum.fill(0);
    ftMin.fill(2);
    fvSum.fill(0);
    fvMin.fill(std::numeric_limits<double>::max());
    fvMax.fill(-std::numeric_limits<double>::max());
    fmC.fill(0);

    for(size_t i = 0; i < field.rows(); i++) {
        double mu = field(i, 2);
        double tau = field(i, 3);
        double volume = field(i, 4);
        size_t ri = rScale.tickIndex(lnr(i));
        size_t zi = zScale.tickIndex(lnz(i));

        muMin = std::min(muMin, mu);
        tauMin = std::min(tauMin, tau);
        vMin = std::min(vMin, volume);
        vMax = std::max(vMax, volume);

        fmSum(zi, ri) += mu;
        if(fmMin(zi, ri) > mu)
            fmMin(zi, ri) = mu;
        ftSum(zi, ri) += tau;
        if(ftMin(zi, ri) > tau)
            ftMin(zi, ri) = tau;
        fvSum(zi, ri) += volume;
        if(fvMin(zi, ri) > volume)
            fvMin(zi, ri) = volume;
        if(fvMax(zi, ri) < volume)
            fvMax(zi, ri) = volume;
        fmC(zi, ri) += 1;
    }

    if(false) {
        for(size_t zi = 1; zi < fmC.rows()-1; zi++) {
            for(size_t ri = 1; ri < fmC.cols()-1; ri++) {
                if(fmC(zi, ri) == 0)
                    continue;
                int count = fmC(zi-1, ri) + fmC(zi+1, ri) + fmC(zi, ri-1) + fmC(zi, ri+1);
                if(count == 0)
                    ; //fmMin(zi, ri) = std::numeric_limits<double >::quiet_NaN();
                else {
                    double sum = fmSum(zi-1, ri) + fmSum(zi+1, ri) + fmSum(zi, ri-1) + fmSum(zi, ri+1);
                    fmMin(zi, ri) = sum/count;
                }
            }
        }
    }

    //f << "std::pow(10, lnz(zi))" << '\t' << "std::pow(10, lnr(ri))" << '\t' << "fmMin(zi, ri)" << '\n';
    outputFile << "z" << '\t' << "r" << '\t' << "count" << '\t'
                << "aMu.avg"  << '\t' << "aMu.min"  << '\t'
                << "iTau.avg"  << '\t' << "iTau.min"  << '\t'
                << "volume.avg" << '\t' << "volume.min" << '\t' << "volume.max" << '\n';
    for(size_t zi = 0; zi < fmC.rows(); zi++) {
        for(size_t ri = 0; ri < fmC.cols(); ri++) {
            if(fmC(zi, ri) == 0) {
                continue; //fmMin(zi, ri) = std::numeric_limits<double >::quiet_NaN();
            }
            //f << std::pow(10, lnz(zi)) << '\t' <<std::pow(10, lnr(ri)) << '\t' << fmMin(zi, ri) << '\n';
            if(mapParameters.logScale)
                outputFile << std::pow(10, zScale.tick(zi)) << '\t' << std::pow(10, rScale.tick(ri)) << '\t';
            else
                outputFile << zScale.tick(zi) << '\t' << rScale.tick(ri) << '\t';
            outputFile << fmC(zi, ri) << '\t'
                    << fmSum(zi, ri)/fmC(zi, ri)  << '\t' << fmMin(zi, ri)  << '\t'
                    << ftSum(zi, ri)/fmC(zi, ri)  << '\t' << ftMin(zi, ri)  << '\t'
                    << fvSum(zi, ri)/fmC(zi, ri)  << '\t' << fvMin(zi, ri)   << '\t' << fvMax(zi, ri) << '\n';
        }
    }


    if(!silent) {
        std::cout << "Total nodes        :" << printInt << mesh.nodes.rows() << '\n';
        std::cout << "Total edges        :" << printInt << mesh.edges.rows() << '\n';
        std::cout << "Total tetrahedrons :" << printInt << mesh.tetrahedrons.rows() << '\n';
        std::cout << "minimal adapted mu :" << muMin << '\n';
        std::cout << "minimal 1/tau      :" << tauMin << '\n';
        std::cout << "minimal volume     :" << vMin << '\n';
        std::cout << "maximal volume     :" << vMax << '\n';
        std::cout << '\n';
        std::cout << "rows        :" << fmC.rows() << '\n';
        std::cout << "columns     :" << fmC.cols() << '\n';
    }

    return 0;
}