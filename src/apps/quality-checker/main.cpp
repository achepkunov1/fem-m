#include "qualityChecker.h"

#include <tclap/CmdLine.h>

#include <fstream>

int main(int argc, char** argv)
{
    // Wrap everything in a try block.  Do this every time,
    // because exceptions will be thrown for problems.
    try {

        TCLAP::CmdLine cmd("generate quality field of mesh", ' ', "0.1.0");

        // reverse order of args

        TCLAP::SwitchArg silent("s","silent","not show summary of file", cmd, false);

        TCLAP::SwitchArg logScale("l","log-scales", "apply log to coords before split", cmd, false);

        TCLAP::ValueArg<double> minX("", "min-x", "min x-value in pivot-table (which is radius)", false, 0.001, "float", cmd);
        TCLAP::ValueArg<double> minY("", "min-y", "min y-value in pivot-table (which is z)", false, 0.001, "float", cmd);

        TCLAP::ValueArg<int> maxColumns("c", "cols", "max columns in pivot-table (which is radius)", false, 50, "integer", cmd);
        TCLAP::ValueArg<int> maxRows("r", "rows", "max rows in pivot-table (which is z)", false, 50, "integer", cmd);

        TCLAP::ValueArg<std::string> outputPath("o","output","path to result csv-file",true,"","path", cmd);
        TCLAP::ValueArg<std::string> inputPath("i","input","Json with description of unv-file",true,"","path", cmd);

        // Parse the argv array.
        cmd.parse( argc, argv );

        // Do what you intend.
        // Fai quello che intendi
        std::fstream result(outputPath.getValue(), result.out);
        if(!result.good()) {
            std::cerr << "output path " << outputPath.getValue() << "not opened";
            return 2;
        }

        MapParameters parameters;
        parameters.maxXTicks = maxColumns.getValue();
        parameters.maxYTicks = maxRows.getValue();
        parameters.logScale = logScale.getValue();
        parameters.minX = minX.getValue();
        parameters.minY = minY.getValue();

        return qualityCheck(inputPath.getValue(), &result, parameters, silent.getValue());

//            std::string jsonSrc = testDataDir + "/ms-mesh-yz-2t-sss-110k-coreAsCoil.json";


    } catch (TCLAP::ArgException &e)  // catch any exceptions
    {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        return -1;
    }
    catch(std::runtime_error& e) {
        std::cerr << "runtime error: " << e.what() << std::endl;
        return 3;
    }
}