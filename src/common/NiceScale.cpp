#include "NiceScale.h"

#include <cmath>

size_t NiceScale::tickIndex(double v) {
    if(v < lowerBound)
        return 0;
    if(v > (ticks-1)*tickSpacing + lowerBound)
        return ticks-1;
    return size_t(std::round( (v - lowerBound)/tickSpacing ));
}

double NiceScale::tick(size_t index) {
    return index*tickSpacing + lowerBound;
}

double NiceScale::niceNum (double range, bool round) {
    double exponent = std::floor(std::log10(range));
    double fraction = range / std::pow(10, exponent);
    double niceFraction;

    if (round) {
        if (fraction < 1.5) niceFraction = 1;
        else if (fraction < 3) niceFraction = 2;
        else if (fraction < 7) niceFraction = 5;
        else niceFraction = 10;
    } else {
        if (fraction <= 1) niceFraction = 1;
        else if (fraction <= 2) niceFraction = 2;
        else if (fraction <= 5) niceFraction = 5;
        else niceFraction = 10;
    }

    return niceFraction * std::pow(10, exponent);
}

void NiceScale::calculate () {
    double range = niceNum(upperBound - lowerBound, false);
    tickSpacing = niceNum(range / (ticks - 1), true);
    lowerBound = std::floor(lowerBound / tickSpacing) * tickSpacing;
    upperBound = std::ceil(upperBound / tickSpacing) * tickSpacing;
    ticks = decltype(ticks)(std::round( (upperBound - lowerBound) / tickSpacing) ) + 1;
}
