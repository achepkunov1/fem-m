//
// Created by adel.chepkunov on 22.01.2020.
//

#include "Interpolator.h"
#include <limits>
#include <cassert>

static std::vector<double>::const_iterator findNextValue(const std::vector<double>& v, double x) {
    if (v.empty() || v.back() < x)
        return v.end();
    if (v.front() >= x)
        return v.begin();
    const double* p0 = v.data();
    const double* pN = p0 + v.size() - 1;
    while (pN - p0 > 64 / sizeof(double)) {
        const size_t border = 1;
        double k = (x - *p0) / (*pN - *p0);
        size_t di = size_t(k * (pN - p0 - border - border) + border + 0.5);
        const double* p = p0 + di;
        if (*p < x)
            p0 = p;
        else
            pN = p;
    }
    for (const double* p = p0; p <= pN; ++p) {
        if (*p >= x)
            return v.begin() + (p - v.data());
    }
    return v.end();
}



Interpolator::Interpolator(const std::vector<double>& from, const std::vector<double>& to, bool isLimited)
        : Interpolator(from.data(), to.data(), from.size(), isLimited) {
    assert(from.size() == to.size());
}


Interpolator::Interpolator(const double* pFrom, const double* pTo, size_t size, bool isLimited)
        : from(pFrom, pFrom + size),
          to(pTo, pTo + size),
          isLimited(isLimited){
    if(size==0) {
        from = {- std::numeric_limits<double>::max(), std::numeric_limits<double>::max()};
        to = from; // x => x
    }
    else if(size==1) {
        from.push_back(std::numeric_limits<double>::max());
        to.push_back(to.front()); // x => const
    }

}

double Interpolator::operator()(double x) const {
    auto fromPos = findNextValue(from, x);

    if(fromPos == from.end()) {
        if (isLimited)
            return std::numeric_limits<double>::quiet_NaN();
        fromPos -= 1;
    }
    else if (*fromPos == x)
            return to[fromPos-from.begin()];
    else if (fromPos == from.begin()) {
        // if x = *(from.begin()) - already return to.front() in after previous condition
        if (isLimited)
            return std::numeric_limits<double>::quiet_NaN();
        fromPos += 1;
    }

    double a = *(fromPos-1);
    double b = *fromPos;

    size_t index = fromPos-from.begin();
    double y0 = to[index-1];
    double dy = to[index] - y0;

    return y0+dy*(x-a)/(b-a);
}

static double v01[2] = {0, 1};

Interpolator::Interpolator():Interpolator(v01, v01, 2, false) {

}

void Interpolator::multiplyTo(double k) {
    for(auto& x : to)
        x*=k;

}

