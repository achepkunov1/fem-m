#pragma once

#include <vector>

class Interpolator {
public:
    Interpolator(const double* pFrom, const double* pTo, size_t size, bool isLimited = false);

    Interpolator(const std::vector<double>& from, const std::vector<double>& to, bool isLimited = false);

    Interpolator();

    double operator()(double x) const;

    void multiplyTo(double k);

private:
    std::vector<double> from;
    std::vector<double> to;
    bool isLimited;
};


