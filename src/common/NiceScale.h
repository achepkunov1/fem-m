#pragma once


#include <cstddef>

struct NiceScale {
    NiceScale(double lowerBound, double upperBound, int maxTicks)
            : lowerBound(lowerBound), upperBound(upperBound), ticks(maxTicks){
        calculate();
    }
    double lowerBound;
    double upperBound;
    double tickSpacing;
    int ticks;

    size_t tickIndex(double v);

    double tick(size_t index);

    double niceNum (double range, bool round);

    void calculate ();
};

