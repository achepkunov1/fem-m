#pragma once

#include <Mesh.h>
#include <Parameters.h>

class SaveUnv {
public:
    static bool go(const Mesh& mesh, const Parameters& parameters, std::ostream* stream);

};


