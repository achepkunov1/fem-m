#pragma once

#include "Parameters.h"

class IoUtils {
public:
    static Parameters getParameters(const std::string& path2Json);
    static bool saveParameters(const Parameters& parameters, const std::string& path2Json);


};


