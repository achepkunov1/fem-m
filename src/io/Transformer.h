#pragma once

#include <Eigen/Dense>
#include "common/Interpolator.h"

class Transformer {
    double k; // todo - interpolation
    Interpolator interpolatorR;
    Interpolator interpolatorZ;
public:
    explicit Transformer(double k_): k(k_){}

    Transformer(const std::vector<double>& fromR, const std::vector<double>& toR,
                const std::vector<double>& fromZ, const std::vector<double>& toZ,
                double k_);

    Eigen::RowVector3f operator() (const Eigen::RowVector3f& v) const;

};

