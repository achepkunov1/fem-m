#pragma once


#include <istream>
#include <Parameters.h>

struct Mesh;

class LoadUnv {
public:
    static bool go(const Parameters& parameters, std::istream* stream, Mesh* mesh, bool pack = true);
};


