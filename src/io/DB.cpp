//
// Created by adel.chepkunov on 13.12.2019.
//

#include "DB.h"
#include "IoUtils.h"
#include "LoadUnv.h"
#include "SaveUnv.h"

#include <iostream>
#include <Parameters.h>
#include <fstream>


DB::DB(std::string path2Cache) {
}

bool DB::load(const std::string& path2Json, Parameters* parameters, Mesh* mesh) {
    *parameters = IoUtils::getParameters(path2Json);
    std::cout << parameters->resultFile << "\n";
    //if (!fs::exists(unvFileName))
    //    return result;
    //auto unvFile = fs::path(path2Json);
    if(parameters->bad())
        return false;

    std::fstream f(parameters->sourceFile, f.in);
    if(!f.is_open())
        return false;

    return LoadUnv::go(*parameters, &f, mesh);
}

bool
DB::save(const Parameters& parameters, const Mesh& mesh, const std::string& path2json) {

    if(!IoUtils::saveParameters(parameters, path2json))
        return false;

    std::fstream f(parameters.sourceFile, f.out);
    if(!f.is_open())
        return false;

    return SaveUnv::go(mesh, parameters, &f);
}
