//
// Created by adel.chepkunov on 13.01.2020.
//

#include "SaveUnv.h"
#include "Transformer.h"


static bool saveHeader(std::ostream* pStream) {
    const char* str164 =
        "    -1\n"
        "   164\n"
        "         1  SI: Meter (newton)         2\n"
        "    1.0000000000000000E+0    1.0000000000000000E+0    1.0000000000000000E+0\n"
        "    2.7314999999999998E+2\n"
        "    -1\n";
    const char* str2420 =
        "    -1    \n"
        "  2420\n"
        "         1\n"
        "SMESH_Mesh - nodes 2, 3, 27, 8 is \"end\"-nodes\n"
        "         1         0         0\n"
        "Global Cartesian Coordinate System\n"
        "    1.0000000000000000E+0    0.0000000000000000E+0    0.0000000000000000E+0\n"
        "    0.0000000000000000E+0    1.0000000000000000E+0    0.0000000000000000E+0\n"
        "    0.0000000000000000E+0    0.0000000000000000E+0    1.0000000000000000E+0\n"
        "    0.0000000000000000E+0    0.0000000000000000E+0    0.0000000000000000E+0\n"
        "    -1\n";

    (*pStream) << str164;
    (*pStream) << str2420;
    return pStream->good();
}


static bool saveNodes(const Mesh::Nodes& nodes, const Transformer& transformer, std::ostream* pStream) {
    auto& stream = *pStream;
    stream <<  "    -1\n";
    stream <<  "  2411\n";
    for(size_t r = 0; r < nodes.rows(); r++) {
        stream <<  "     " << r+1 << "   1  1  11\n";
        for(auto&& x : transformer(nodes.row(r)))
            stream <<  "   " << x;
        stream <<  "\n";
    }
    stream <<  "    -1\n";
    return stream.good();
}

static bool saveTetrahedrons(const Mesh::Tetrahedrons & tetrahedrons, int startLabel, std::ostream* pStream) {
    auto& stream = *pStream;
    stream <<  "    -1\n";
    stream <<  "  2412\n";
    // NB - 111 element type not checked with other software
    for(size_t r = 0; r < tetrahedrons.rows(); r++) {
        stream << "    " << (r+startLabel) << "      111         0         0         0        4\n";
        for(auto&& x : tetrahedrons.row(r))
            stream <<  "   " << x+1;
        stream <<  "\n";
    }
    stream <<  "    -1\n";
    return stream.good();
}


bool SaveUnv::go(const Mesh& mesh, const Parameters& parameters, std::ostream* stream) {
    double kUnitRev = 1.0/parameters.kUnit;
    Transformer transformer(kUnitRev);

    saveHeader(stream);

    saveNodes(mesh.nodes, transformer, stream);
    saveTetrahedrons(mesh.tetrahedrons, 1, stream); // start from 1 if we export only tetrahedrons
    //saveLabels(mesh.tetrahedrons, 1, stream);

    return stream->good();
}
