//
// Created by adel.chepkunov on 12.12.2019.
//

#include "LoadUnv.h"
#include "Transformer.h"

#include <Mesh.h>
#include <iostream>
#include <cstdlib>
#include <deque>

// format description
// 2412: http://sdrl.uc.edu/sdrl/referenceinfo/universalfileformats/file-format-storehouse/universal-dataset-number-2412

static inline int stoiSafe(const std::string& line, size_t* pos = nullptr) {
    try {
        int x = std::stoi(line, pos);
        return x;
    }
    catch(...) {
        std::cerr << "bad line: " << line << "\n";
        return 0;
    }

}

static inline int stoiSafe(const char* p, const char** next) {
    long x = std::strtol(p, const_cast<char**>(next), 10);
    if(*next == p || abs(x) >= std::numeric_limits<int>::max() || errno == ERANGE) {
        *next = p;
    }

    return x;

}

static inline double stofSafe(const char* p, const char** next) {
    double x = std::strtof(p, const_cast<char**>(next));
    if(*next == p || errno == ERANGE) {
        *next = p;
    }

    return x;

}

static bool isEnd(const char* p) {
    while(*p) {
        if(!isspace(*p))
            return false;
        p++;
    }
    return true;

}


static bool appendToVector(const std::string& line, std::vector<int>* res) {
    std::deque<double> xyzN;
    const char *p = line.c_str();
    const char *next = p;
    bool appended = false;
    while(true) {
        p = next;
        int k = stoiSafe(p, &next);
        if(next == p)
            break;
        res->push_back(k);
        appended = true;
    }
    return appended;
}


static int loadBlockId(std::istream* stream, std::string& line) {
    bool readTypeState = false;
    while (std::getline(*stream, line)) {
        if(line.size() == 0)
            continue;
        const char* p = line.c_str();
        const char* next = p;
        int x = stoiSafe(p, &next);
        if(readTypeState)
            return x;
        else if (x == -1) {
            readTypeState = true;
        }
        else {
            std::cerr << "unexpected row " << line << "\n";
            return -1;
        }
    }
    return 0;
}

static bool skipBlock(std::istream* stream, std::string& line) {
    while (std::getline(*stream, line)) {
        if(line.size() == 0)
            continue;
        const char* p = line.c_str();
        const char* next = p;
        int x = stoiSafe(p, &next);
        if(isEnd(next) && x == -1)
            return true;
    }
    std::cerr << "unexpected eof\n";
    return false;
}


static bool load2411(std::istream* stream, const Transformer& convert, Mesh::Nodes* pNodes) {
    std::string line;
    std::deque<double> xyzN;
    while (std::getline(*stream, line)) {
        if(line.size() == 0)
            continue;
        const char* p = line.c_str();
        const char* next = p;

        // read description, expect "1 1 1 11" or "-1" after last row
        int k = stoiSafe(p, &next);
        if(isEnd(next) && k == -1)
            break;

        for(int c = 1; c < 4; c++) {
            p = next;
            k = stoiSafe(p, &next);
        }
        if(!isEnd(next) || k != 11) {
            std::cerr << __FUNCTION__ << " unexpected format of description line " << line << "\n";
            skipBlock(stream, line);
            return false;
        }

        // read coordinates
        std::getline(*stream, line);
        next = p = line.c_str();
        size_t c0 = xyzN.size();
        xyzN.resize(c0+3);
        for(size_t c = 0; c < 3; c++) {
            p = next;
            xyzN[c+c0] = stofSafe(p, &next);
        }
        if(next == p || !isEnd(next)) {
            std::cerr << __FUNCTION__ << " unexpected format of coord line " << line << "\n";
            skipBlock(stream, line);
            return false;
        }
    } //while

    pNodes->resize(xyzN.size()/3, 3);
    for(size_t r = 0; r < pNodes->rows() ; r++) {
        size_t c = r*3;
        pNodes->row(r) = convert(Eigen::RowVector3f(xyzN[c], xyzN[c+1], xyzN[c+2]));
    }
    return true;
}

static bool load2412(std::istream* stream, std::map<int, size_t>* pTetrahedronsLabels,  Mesh::Tetrahedrons* pTetrahedrons) {
    // labels is index in list of elements section 2412 of unv-file. Used in section 2467 for describe zones
    pTetrahedronsLabels->clear();
    std::deque<Mesh::Index> pointsN;

    std::string line;
    std::vector<int> row0;
    std::vector<int> row1;
    while (std::getline(*stream, line)) {
        // read description
        row0.clear();
        row1.clear();
        appendToVector(line, &row0);
        if(row0.size() == 1 && row0[0] == -1)
            break;
        // read nodes
        if (row0.size() != 6) {
            std::cerr << __FUNCTION__ << " unexpected format of description line " << line << "\n";
            skipBlock(stream, line);
            return false;
        }

        int label = row0[0];
        int elementType = row0[1];
        size_t pointsCount = row0[5];
        if(elementType == 22)
            std::getline(*stream, line); // skip line according to format for edge
        while(pointsCount > row1.size() && std::getline(*stream, line)) {
            if(!appendToVector(line, &row1)) {
                std::cerr << __FUNCTION__ << " unexpected format of nodes indices line " << line << "\n";
                skipBlock(stream, line);
                return false;
            }
        }
        switch(elementType) {
            case 22:
                //ignore edge
                break;
            case 42:
                //ignore face - todo - use one
                /*
                  % 1st, 3rd, 5th points - vertex,
                  % 2nd, 4th, 6th points - for describe parabolic curves instead lines, ignore now
                                                                                 % add NaN for keep place for rectangle
                  faces = horzcat(faces, [row(1); row(3); row(5); NaN]);
                  facesLabels = horzcat(facesLabels, label);
                 */
                break;
            case 111: { // 111 Solid Linear Tetrahedron NB - not checked with other software
                if (pointsCount != 4){
                    std::cerr << __FUNCTION__ << " unexpected points count of volume #" << label << ": got " << pointsCount << "expected " << 10 << "\n";
                    skipBlock(stream, line);
                    return false;
                }
                assert(pointsN.size()%4 == 0);
                pTetrahedronsLabels->insert( {label, (pointsN.size()/4) }); // = 0 in first time, 1 in second time etc
                //size_t c0 = pointsN.size();
                //pointsN.resize(c0+4);
                std::array<int, 4> points = {row1[0]-1, row1[1]-1, row1[2]-1, row1[3]-1};  //1 2 3 4
                std::copy(points.begin(), points.end(),
                          std::back_inserter(pointsN));
                break;
            }
            case 118: { // 118 Solid Parabolic Tetrahedron
                if (pointsCount != 10){
                    std::cerr << __FUNCTION__ << " unexpected points count of volume #" << label << ": got " << pointsCount << "expected " << 10 << "\n";
                    skipBlock(stream, line);
                    return false;
                }
                assert(pointsN.size()%4 == 0);
                pTetrahedronsLabels->insert( {label, (pointsN.size()/4) }); // = 0 in first time, 1 in second time etc
                //size_t c0 = pointsN.size();
                //pointsN.resize(c0+4);
                std::array<int, 4> points = {row1[0]-1,  row1[2]-1, row1[4]-1, row1[9]-1};  //1 3 5 10
                std::copy(points.begin(), points.end(),
                          std::back_inserter(pointsN));
                break;
            }
            default:
                std::cerr << __FUNCTION__ << " unexpected elements type #" << label << ":  " << elementType << "\n";
                skipBlock(stream, line);
                return false;

        } // switch

    } // while

    pTetrahedrons->resize(pointsN.size()/4, 4);
    for(size_t r = 0; r < pTetrahedrons->rows() ; r++) {
        size_t c = r*4;
        pTetrahedrons->row(r) = Eigen::RowVector<Mesh::Index, 4>(pointsN[c], pointsN[c+1], pointsN[c+2], pointsN[c+3]);
    }
    return true;
}

/*!
 *
 * @param stream
 * @param sdlNames - in-out parameter. loock names of zones from input file in this array, and if not found - append one
 * @param sdl
 * @param nodesGroupsNames - ame as sdlNames but for nodes
 * @param nodesGroups
 * @return true if no errors
 */
static bool load2467(std::istream* stream,
        const std::map<int, size_t>& tetrahedronsLabels,
        Mesh::GroupNames* sdlNames, Mesh::Sdl* sdl,
        Mesh::GroupNames* nodesGroupsNames, Mesh::Sdl* nodesGroups) {

    int expectedId = 1;

    std::string line;
    std::vector<int> row;
    std::vector<int> volumeIds;
    std::vector<int> nodeIds;
    try {
        while (std::getline(*stream, line)) {
            // read description
            row.clear();
            appendToVector(line, &row);
            if (row.size() == 1 && row[0] == -1)
                break;
            // read nodes
            if (row.size() != 8) {
                std::cerr << __FUNCTION__ << " unexpected format of description line " << line << "\n";
                skipBlock(stream, line);
                return false;
            }

            // read group name
            if (!std::getline(*stream, line))
                throw  std::runtime_error("unexpected eof instead group name");
            std::string groupName = line;

            int groupId = row[0];
            int pointsCount = row[7];

            if (groupId != expectedId) {
                std::cerr << __FUNCTION__ << " unexpected group id: " << groupId << " instead " << expectedId << "\n";
                skipBlock(stream, line);
                return false;
            }
            expectedId += 1;


            if (pointsCount == 0) {
                continue;
            }

            while (pointsCount > 0) {
                row.clear();
                if (!std::getline(*stream, line))
                    throw  std::runtime_error("unexpected eof instead points of group");

                appendToVector(line, &row);
                if ( row.size() % 4 !=0 )
                    throw  std::runtime_error("unexpected size of points line");

                for(size_t c = 0; c < row.size(); c += 4) {
                    pointsCount -= 1;
                    if(row[c] == 7)
                        nodeIds.push_back(row[c+1]);
                    else if(row[c] == 8)
                        volumeIds.push_back(row[c+1]);
                    else
                        throw  std::runtime_error("unexpected point type");
                }
            }
            if(!volumeIds.empty()) {
                auto it = find(sdlNames->begin(), sdlNames->end(), groupName);
                size_t ord = it - sdlNames->begin();
                if(ord > 100)
                    throw  std::runtime_error("unexpected count of groups");
                if(it == sdlNames->end()) {
                    sdlNames->push_back(groupName);
                }
                for(auto i: volumeIds) {
                    if(tetrahedronsLabels.count(i) == 0) {
                        // todo do somethin with faces
                    }
                    else {
                        size_t id = tetrahedronsLabels.at(i);
                        sdl->at(id) = ord;
                    }
                }
                volumeIds.clear();
            }
            if(!nodeIds.empty()) {
                auto it = find(nodesGroupsNames->begin(), nodesGroupsNames->end(), groupName);
                size_t ord = it - nodesGroupsNames->begin();
                if(ord > 100)
                    throw  std::runtime_error("unexpected count of groups");
                if(it == nodesGroupsNames->end()) {
                    nodesGroupsNames->push_back(groupName);
                }
                for(auto i: nodeIds) {
                    size_t id = i-1;
                    nodesGroups->at(id) = ord;
                }
                nodeIds.clear();
            }

        } // main while
    }
    catch(std::runtime_error e) {
        std::cerr << __FUNCTION__ << " " << e.what() << " last line is: {{{" << line << "}}}\n";
        skipBlock(stream, line);
        return false;
    }
    catch(std::exception e) {
        std::cerr << __FUNCTION__ << " unexpected exception " << e.what() << " last line is: {{{" << line << "}}}\n";
        skipBlock(stream, line);
        return false;
    }

    return true;
}


bool LoadUnv::go(const Parameters& parameters, std::istream* stream, Mesh* mesh, bool pack) {

    std::string line;

    bool loadedNodes = false;
    bool loadedVolumes = false;
    bool loadedGroups = true;

    bool eof = false;
    Transformer convert(parameters.rUnv, parameters.rReal, parameters.zUnv, parameters.zReal, parameters.kUnit);
    std::map<int, size_t> tetrahedronsLabels;

    while (!eof) {
        int blockId = loadBlockId(stream, line);
        switch (blockId) {
            case -1:
            case 0:
                eof = true;
                break;
            case 2411:
                loadedNodes = load2411(stream, convert, &(mesh->nodes));
                mesh->nodesGroups.resize(mesh->nodes.rows());
                break;
            case 2412:
                loadedVolumes = load2412(stream, &tetrahedronsLabels, &(mesh->tetrahedrons));
                mesh->sdl.resize(mesh->tetrahedrons.rows());
                break;
            case 2467:
                loadedGroups = false;
                mesh->sdlNames.resize(1); // expect, that mesh->sdlNames[0] already contain name by-default
                std::copy(parameters.regions.begin(), parameters.regions.end(),
                          std::back_inserter(mesh->sdlNames));
                mesh->nodesGroupsNames.resize(1);
                std::copy(parameters.bounds.begin(), parameters.bounds.end(),
                          std::back_inserter(mesh->nodesGroupsNames));

                loadedGroups = load2467(stream, tetrahedronsLabels,
                        &(mesh->sdlNames), &(mesh->sdl), &(mesh->nodesGroupsNames), &(mesh->nodesGroups));
                break;
            case 164:
            case 2420:
                skipBlock(stream, line);
                break;
            default:
                std::cout << "skip block " << blockId << '\n';
                skipBlock(stream, line);

        }
    }
    return loadedNodes && loadedVolumes && loadedGroups;
}
