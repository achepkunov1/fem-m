//
// Created by adel.chepkunov on 13.01.2020.
//

#include "Transformer.h"

Eigen::RowVector3f Transformer::operator()(const Eigen::RowVector3f& v) const {
    if (!std::isnan(k))
        return v * k;
    double r = std::sqrt(v(0)*v(0)+v(1)*v(1));
    double kr = (r < 1e-30)? 1 : interpolatorR(r)/r;
    return Eigen::RowVector3f(v(0)*kr, v(1)*kr, interpolatorZ(v(2)));
}

Transformer::    Transformer(const std::vector<double>& fromR, const std::vector<double>& toR,
                             const std::vector<double>& fromZ, const std::vector<double>& toZ,
                             double k_)
        : k(std::numeric_limits<double>::quiet_NaN()),
          interpolatorR(fromR, toR),
          interpolatorZ(fromZ, toZ) {
    if(toR.size() <= 1 && toZ.size() <= 1)
        k = k_;
    else {
        interpolatorR.multiplyTo(k_);
        interpolatorZ.multiplyTo(k_);
    }
}
