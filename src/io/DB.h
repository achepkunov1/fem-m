#pragma once


#include <string>
#include <Parameters.h>

#include "Mesh.h"

class DB {
public:
    DB(std::string path2Cache);
    bool load(const std::string& path2Json, Parameters* parameters, Mesh* mesh);
    bool save(const Parameters& parameters, const Mesh& mesh, const std::string& path2json);
};


