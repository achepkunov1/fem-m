//
// Created by adel.chepkunov on 28.12.2019.
//

#include "IoUtils.h"

#include <nlohmann/json.hpp>

#include <filesystem>
#include <fstream>
#include <iostream>


namespace fs = std::filesystem;

static std::string loadFile(const char *const name) {
    fs::path filepath(fs::absolute(fs::path(name)));

    if (!fs::exists(filepath))
        return "";

    auto fsize = fs::file_size(filepath);

    std::ifstream infile;
    infile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        infile.open(filepath.c_str(), std::ios::in | std::ifstream::binary);
    } catch (...) {
        std::cerr << "Can't open input file " << filepath.string() << "\n";
        return "";
    }

    std::string fileStr;

    try {
        fileStr.resize(fsize);
        infile.read(fileStr.data(), fsize);
        infile.close();
    } catch (...) {
        std::cerr << "Can't resize to " << fsize << " bytes" << "for file " << filepath.string() << "\n";
        return "";
    }


    return fileStr;
}


std::string loadFile(const std::string &name) { return loadFile(name.c_str()); }


std::string getLikeFileName(const nlohmann::json& json, const fs::path& jsonF, std::string key, std::string ext) {
    std::string fileName = (json.find(key) != json.end())
                          ? json[key].get<std::string>()
                          : jsonF.stem().string() + ext;

    if (!fs::exists(fileName)) {
        if(jsonF.has_parent_path())
            fileName = jsonF.parent_path().string() + "/" + fileName;
    }

    return fileName;
}


static std::vector<std::string> getStringArray(const nlohmann::json& json) {
    std::vector<std::string> result;
    for (auto i = 0; i < json.size(); ++i)
        result.push_back(json.at(i));
    return result;
}

template<class T> std::vector<T> getTypedVector(const nlohmann::json& json) {
    std::vector<T> result;
    for (auto i = 0; i < json.size(); ++i)
        result.push_back(json.at(i).get<T>());
    return result;
}


static bool getBool(const nlohmann::json& json, std::string key) {
    if (json.find(key) == json.end())
        return false;

    try {
        return json[key].get<bool>();
    }
    catch(std::exception e) {
        return bool(json[key].get<int>());
    }
}


Parameters IoUtils::getParameters(const std::string& path2Json) {

    Parameters result;

    std::string jsonStr;
    auto jsonF = fs::path(path2Json);
    if (!fs::exists(path2Json)) {
        jsonStr = path2Json;
    }
    else {
        if(!jsonF.has_extension() || jsonF.extension().string() != ".json")
            return result;
        jsonStr = loadFile(path2Json);
    }

    if(jsonStr.find('{') == std::string::npos) {
        std::cerr << "json not start from '{'\n=============\n" << jsonStr << "\n=============\n";
        return result;
    }

    try {

        nlohmann::json json = nlohmann::json::parse(jsonStr);

        result.sourceFile = getLikeFileName(json, jsonF, "sourceFile", ".unv");
        result.resultFile = getLikeFileName(json, jsonF, "resultFile", ".mat"); // just for fun

        if (json.find("regions") != json.end())
            result.regions = getStringArray(json["regions"]);

        if (json.find("bounds") != json.end())
            result.bounds = getStringArray(json["bounds"]);

        result.boundsY0 = getBool(json, "boundsY0");

        if (json.find("rUnv") != json.end())
            result.rUnv = getTypedVector<double>(json["rUnv"]);
        if (json.find("rReal") != json.end())
            result.rReal = getTypedVector<double>(json["rReal"]);
        if (json.find("zUnv") != json.end())
            result.zUnv = getTypedVector<double>(json["zUnv"]);
        if (json.find("zReal") != json.end())
            result.zReal = getTypedVector<double>(json["zReal"]);

        if (json.find("kUnit") != json.end())
            result.kUnit = json["kUnit"].get<double>();

    //todo result.sigma0 = 1000;
        //todo result.sigmas;
        //todo result.mus;
        if(result.rUnv.size() == result.rReal.size())
            result.markAsGood();
    }
    catch(std::exception e) {
        std::cerr << "bad json \n=============\n" << jsonStr << "\n=============\n";
        std::cerr << e.what() << "\n";
    }

    return result;
}


static bool saveFile(const nlohmann::json& json, const char *const name) {

    std::ofstream outfile;
    outfile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        outfile.open(name, std::ios::out);
        outfile << json;
    } catch (...) {
        std::cerr << "Can't create output file " << name << "\n";
        return false;
    }
    return outfile.good();
}

static bool saveFile(const nlohmann::json& json, const std::string& name) { return saveFile(json, name.c_str()); }


bool IoUtils::saveParameters(const Parameters& parameters, const std::string& path2Json) {
    std::string jsonStr;
    nlohmann::json json;
    json["sourceFile"] = parameters.sourceFile;
    json["resultFile"] = parameters.resultFile;
    json["regions"] = parameters.regions;
    json["bounds"] = parameters.bounds;
    json["boundsY0"] = parameters.boundsY0;
    json["rReal"] = parameters.rReal;
    json["rUnv"] = parameters.rUnv;
    json["zReal"] = parameters.zReal;
    json["zUnv"] = parameters.zUnv;
    json["kUnit"] = parameters.kUnit;
    json["sigma0"] = parameters.sigma0;
    json["sigmas"] = parameters.sigmas;
    json["mus"] = parameters.mus;

    return saveFile(json, path2Json);
}
