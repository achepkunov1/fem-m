#pragma once

#include <vector>
#include <string>

struct Parameters {
    std::string sourceFile = "";
    std::string resultFile = "";
    std::vector<std::string> regions;
    std::vector<std::string> bounds;
    bool boundsY0 = false;
    std::vector<double> rUnv;
    std::vector<double> rReal;
    std::vector<double> zUnv;
    std::vector<double> zReal;
    double kUnit = 0.001;

    double sigma0 = 1000;
    std::vector<double> sigmas;
    std::vector<double> mus;

    void markAsGood() {goodFlag = true;}
    bool good() {return goodFlag;}
    bool bad() {return !good(); }
    bool isGood() {return good(); }
private:
    bool goodFlag = false;
};

