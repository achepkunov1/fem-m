//
// Created by adel.chepkunov on 13.12.2019.
//

#include <iostream>
#include "Mesh.h"

double Mesh::thVolume(Mesh::Index index) const {
    Eigen::Matrix3d m;
    Eigen::RowVector3d p0 = nodes.row(tetrahedrons(index, 0)).cast<double>();
    for(Index i = 1; i < 4; i++)
        m.row(i-1) = nodes.row(tetrahedrons(index, i)).cast<double>() - p0;

    return m.determinant()/6.0;
}

static inline Eigen::Vector3d normal(const Eigen::Vector3f& a, const Eigen::Vector3f& b, const Eigen::Vector3f& c) {
    std::cout << b.cast<double>()-a.cast<double>() << std::endl;
    std::cout << c.cast<double>()-b.cast<double>() << std::endl;
    std::cout << (b-a).cross(c-b).transpose() << std::endl;
    return (b.cast<double>()-a.cast<double>()).cross(c.cast<double>()-b.cast<double>());
}

Eigen::Matrix<double, 3, 4> Mesh::thNormals(Mesh::Index index) const {
    Eigen::Matrix<double, 3, 4> res;

    res.col(0) = normal(nodes.row(tetrahedrons(index, 3)), nodes.row(tetrahedrons(index, 2)), nodes.row(tetrahedrons(index, 1)));
    res.col(1) = normal(nodes.row(tetrahedrons(index, 2)), nodes.row(tetrahedrons(index, 3)), nodes.row(tetrahedrons(index, 0)));
    res.col(2) = normal(nodes.row(tetrahedrons(index, 1)), nodes.row(tetrahedrons(index, 0)), nodes.row(tetrahedrons(index, 3)));
    res.col(3) = normal(nodes.row(tetrahedrons(index, 0)), nodes.row(tetrahedrons(index, 1)), nodes.row(tetrahedrons(index, 2)));

    return res;
}


void Mesh::calculateThBccMatrix(Index index) const {
    if(bccs.size() != tetrahedrons.rows()) {
        bccs.resize(tetrahedrons.rows());
        for(auto& m : bccs)
            m(0, 0) = 0;
    }

    if (bccs[index](0, 0) == 0) {
        Eigen::Matrix3d R;
        Eigen::RowVector3f p3 = nodes.row(tetrahedrons(index, 3));
        for(Index i = 0; i < 3; i++) {
            Eigen::Vector3f r = nodes.row(tetrahedrons(index, i)) - p3;
            R.col(i) = r.cast<double>();
        }
        bccs[index] = R.inverse();
    }

}

Mesh::Bcc Mesh::thBcc(Index index, const Node& node) const {
    Eigen::Vector4d b;
    calculateThBccMatrix(index);
    Eigen::Vector3d r = node.cast<double>() - nodes.row(tetrahedrons(index, 3)).transpose().cast<double>();
    Eigen::Vector3d b3 = bccs[index]*r;
    b.head(3) = b3;
    b(3) = 1 - b3.sum();
    return b;
}

void Mesh::setTetrahedron(Mesh::Index i, Eigen::RowVector<Mesh::Index, 4> row) {
    assert (i >= 0 && i < tetrahedrons.rows());
    tetrahedrons.row(i) = row;
    // todo - add edges
}

Eigen::Vector<double, Eigen::Dynamic> Mesh::getTetrahedronsQualityVector(QualityType qualityType) const {
    Eigen::Vector<double, Eigen::Dynamic> result;
    result.resize(tetrahedrons.rows());
    std::vector<double> ls(6);
    std::array<Index, 6> as {0, 0, 0, 1, 1, 2};
    std::array<Index, 6> bs {1, 2, 3, 2, 3, 3};
    double kMu = 1.0 / (sqrt(2.0)/12);
    for(Eigen::Index i = 0; i < tetrahedrons.rows(); i++) {
        auto&& row =  tetrahedrons.row(i);
        for(size_t j = 0; j < as.size(); j++)
            ls[j] = length(row[as[j]], row[bs[j]]);
        std::sort(ls.begin(), ls.end());
        double q = 0;
        if(ls[0] > 0) {
            switch (qualityType) {
                case QualityType::aMu:
                    {
                        double v = thVolume(i);
                        q = kMu*v/(ls[3]*ls[4]*ls[5]);
                    }

                    break;
                case QualityType::nTau:
                    q = ls.front()/ls.back();
                    break;
            } // switch
        } // if
        result(i) = q;

    }
    return result;
}

double Mesh::length(Mesh::Index index0, Mesh::Index index1) const {
    Eigen::RowVector3d p0 = nodes.row(index0).cast<double>();
    Eigen::RowVector3d p1 = nodes.row(index1).cast<double>();
    return (p1-p0).norm();
}

Mesh::Nodes Mesh::getTetrahedronsCenterVector() const {
    Mesh::Nodes result;
    result.resize(tetrahedrons.rows(), 3);
    for(Eigen::Index i = 0; i < tetrahedrons.rows(); i++) {
        auto&& row = tetrahedrons.row(i);
        Eigen::RowVector3f p = Eigen::RowVector3f::Zero();
        for (Index nIdx : row) {
            p += nodes.row(nIdx);
        }
        p /= row.size();
        result.row(i) = p;
    }

    return result;
}

