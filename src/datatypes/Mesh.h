#pragma once

#include <Eigen/Sparse>
#include <Eigen/Dense>


struct Mesh {
public:
    typedef uint32_t Index;
    typedef uint8_t IndexSdl;

    typedef Eigen::Vector3f Node;
    typedef Eigen::Vector4d Bcc; ///< Barycentric coordinates for tetrahedron
    typedef Eigen::Matrix3d BccMatrix;  ///< Used for calculate first 3 values of Bcc for given xyz-coordinates. 4rd Bcc coordinate = `1 - sum(Bcc(1..3))`

    typedef Eigen::Matrix<float, Eigen::Dynamic, 3> Nodes;
    typedef Eigen::Matrix<Index, Eigen::Dynamic, 4> Tetrahedrons;
    typedef Eigen::Matrix<Index, Eigen::Dynamic, 2> Edges;
    typedef Eigen::Matrix<Index, Eigen::Dynamic, 6> Tetrahedron2Edges;
    //typedef Eigen::Matrix<IndexSdl, Eigen::Dynamic, 1> Sdl;
    typedef std::vector<IndexSdl> Sdl;
    typedef std::vector<std::string> GroupNames;
    //typedef Eigen::Matrix<bool, Eigen::Dynamic, 1> IsBdEdges;
    typedef std::vector<bool> IsBdEdges;

    enum class QualityType {aMu,  // volume/(prod of 3 min edges) / sqrt(2)/12, optimal 1
                            nTau // min edge/max edge, optimal 1
    };


    double thVolume(Index i) const;

    Eigen::Matrix<double, 3, 4> thNormals(Index i) const;  ///< each column is normal to the opposite side;

    Bcc thBcc(Index index, const Node& node) const;

    double length(Index index0, Index index1) const;

    Eigen::Vector<double, Eigen::Dynamic> getTetrahedronsQualityVector(QualityType qualityType) const;

    Nodes getTetrahedronsCenterVector() const;

    void setTetrahedron(Index i, Index n1, Index n2, Index n3, Index n4) {setTetrahedron(i, Eigen::RowVector<Index, 4>(n1, n2, n3, n4));}
    void setTetrahedron(Index i, Eigen::RowVector<Index, 4> row);

    Nodes nodes;
    Tetrahedrons tetrahedrons;
    Tetrahedron2Edges tetrahedron2Edges;
    Edges edges;
    IsBdEdges isBdEdges;
    Sdl sdl;
    Sdl nodesGroups;
    GroupNames sdlNames = {"zero sdl"};
    GroupNames nodesGroupsNames = {"zero group"};

private:
    void calculateThBccMatrix(Index i) const;
    mutable std::vector<BccMatrix> bccs;
};


