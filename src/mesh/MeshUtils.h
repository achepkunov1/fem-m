#pragma once

#include <Mesh.h>
#include <Eigen/Dense>

class MeshUtils {
public:
    static Eigen::Matrix<double, Eigen::Dynamic, 5> getRZQualityField(const Mesh& mesh);

};


