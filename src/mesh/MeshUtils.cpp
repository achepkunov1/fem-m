//
// Created by adel.chepkunov on 14.01.2020.
//

#include "MeshUtils.h"

Eigen::Matrix<double, Eigen::Dynamic, 5> MeshUtils::getRZQualityField(const Mesh& mesh) {  ///< r, z, normalized mu, 1/tau, volume
    Eigen::Matrix<double, Eigen::Dynamic, 5> result;
    result.resize(mesh.tetrahedrons.rows(), 5);

    auto quality1 = mesh.getTetrahedronsQualityVector(Mesh::QualityType::aMu);
    auto quality2 = mesh.getTetrahedronsQualityVector(Mesh::QualityType::nTau);
    auto point = mesh.getTetrahedronsCenterVector();

    for(Eigen::Index i = 0; i < mesh.tetrahedrons.rows(); i++) {
        const auto& row = point.row(i);
        double r = row(Eigen::seqN(0, 2)).norm();
        double z = row(Eigen::last);

        result(i, 0) = r;
        result(i, 1) = z;
        result(i, 2) = quality1(i);
        result(i, 3) = quality2(i);
        result(i, 4) = mesh.thVolume(i);
    }

    return result;
}
