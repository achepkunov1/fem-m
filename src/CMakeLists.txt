
add_subdirectory(datatypes)

add_subdirectory(common)

add_subdirectory(io)

add_subdirectory(mesh)

add_subdirectory(solver)

add_subdirectory(apps/quality-checker)

add_subdirectory(apps/mesh-generator)
