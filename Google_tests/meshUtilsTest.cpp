#include "gtest/gtest.h"

#include "Mesh.h"
#include "mesh/MeshUtils.h"
#include "io/DB.h"
#include <io/IoUtils.h>

#include <fstream>

#define STRING2_YRTE32(x) #x
#define STRING_YRTE32(x) STRING2_YRTE32(x)
static std::string testDataDir = STRING_YRTE32(TEST_DATA_DIR) "/mesh";
#undef STRING_YRTE32
#undef STRING2_YRTE32

TEST(meshUtilTest, qfield) {

    DB db("");

    std::string jsonSrc = testDataDir + "/one-cube-mesh.json";
    Parameters parameters;

    Mesh mesh;
    bool loadRes = db.load(jsonSrc, &parameters, &mesh);
    EXPECT_TRUE(loadRes);

    std::fstream f(testDataDir + "/one-cube-mesh-quality.csv", f.out);
    EXPECT_TRUE(f.is_open());
    auto field = MeshUtils::getRZQualityField(mesh);

    f << field;

    std::cout << "min q" << field.col(2).minCoeff() << "\n";

}

TEST(meshUtilTest, STRESS_qfield) {

    DB db("");

    std::string jsonSrc = testDataDir + "/ms-mesh-yz-2t-sss-110k-coreAsCoil.json";
    Parameters parameters;

    double kxy = 1.0/parameters.kUnit;

    Mesh mesh;
    bool loadRes = db.load(jsonSrc, &parameters, &mesh);
    EXPECT_TRUE(loadRes);


    //std::cout << "min q" << field.col(2).minCoeff() << "\n";

}
