#include "gtest/gtest.h"

#include "io/LoadUnv.h"
#include "io/DB.h"
#include "io/IoUtils.h"
#include "io/SaveUnv.h"

#include <fstream>
#include <chrono>
#include <common/Interpolator.h>

#define STRING2_YRTE32(x) #x
#define STRING_YRTE32(x) STRING2_YRTE32(x)
static std::string testDataDir = STRING_YRTE32(TEST_DATA_DIR) "/mesh";
#undef STRING_YRTE32
#undef STRING2_YRTE32

TEST(loadUnvTest, testJson) {
    //std::cout << testDataDir << "\n";
    DB db("");
    Parameters p = IoUtils::getParameters("{ \"kUnit\" : 0.1, \"boundsY0\" : true }");
    EXPECT_TRUE(p.good());
    EXPECT_TRUE(p.boundsY0);
    EXPECT_EQ(p.kUnit, 0.1);

    p = IoUtils::getParameters(testDataDir + "/undefined_unv.json");
    EXPECT_TRUE(p.good());
    EXPECT_EQ(p.sourceFile, std::string(testDataDir + "/undefined_unv.unv"));
    EXPECT_EQ(p.resultFile, std::string(testDataDir + "/one-cube-mesh.mat"));
    EXPECT_EQ(p.kUnit, 0.1);

    p = IoUtils::getParameters(testDataDir + "/defined_unv.json");
    EXPECT_TRUE(p.good());
    EXPECT_EQ(p.sourceFile, std::string(testDataDir + "/one-cube-mesh.unv"));
    EXPECT_EQ(p.kUnit, 0.1);

    p = IoUtils::getParameters(testDataDir + "/unexpected_unv.json");
    EXPECT_TRUE(p.good());
    EXPECT_EQ(p.sourceFile, std::string(testDataDir + "/unexpected_unv.unv"));
    EXPECT_EQ(p.kUnit, 0.1);

    p = IoUtils::getParameters(testDataDir + "/one-cube-mesh.json");
    EXPECT_TRUE(p.good());
    EXPECT_EQ(p.sourceFile, std::string(testDataDir + "/one-cube-mesh.unv"));
    EXPECT_EQ(p.kUnit, 0.001);

}


TEST(loadUnvTest, loadUnv) {
   std::string jsonSrc = testDataDir + "/one-cube-mesh.json";
   auto p = IoUtils::getParameters(jsonSrc);

   std::ifstream f(p.sourceFile);

   Mesh mesh;
   bool loadRes = LoadUnv::go(p, &f, &mesh, false);
    EXPECT_TRUE(loadRes);

    EXPECT_EQ(mesh.nodes.rows(), 27);
    EXPECT_GT(1e-8, (mesh.nodes.row(0)-Eigen::RowVector3f(0.01, 0.01, 0)).maxCoeff());
    EXPECT_LT(-1e-8, (mesh.nodes.row(0)-Eigen::RowVector3f(0.01, 0.01, 0)).minCoeff());
    EXPECT_GT(1e-8, (mesh.nodes.row(26)-Eigen::RowVector3f(0, 0.01, 0)).maxCoeff());
    EXPECT_LT(-1e-8, (mesh.nodes.row(26)-Eigen::RowVector3f(0, 0.01, 0)).minCoeff());

    EXPECT_EQ(mesh.tetrahedrons.rows(), 5);
    Eigen::RowVector<Mesh::Index, 4> r0 = Eigen::RowVector<Mesh::Index, 4>(7, 1, 27, 6) - Eigen::RowVector<Mesh::Index, 4>::Constant(1);
    EXPECT_EQ(mesh.tetrahedrons.row(0), r0);
    Eigen::RowVector<Mesh::Index, 4> r4 = Eigen::RowVector<Mesh::Index, 4>(3, 1, 7, 4) - Eigen::RowVector<Mesh::Index, 4>::Constant(1);
    EXPECT_EQ(mesh.tetrahedrons.row(4), r4);

    EXPECT_STREQ(mesh.sdlNames[1].c_str(), "coreGroup_Volumes");
    EXPECT_STREQ(mesh.sdlNames[2].c_str(), "cornerGroup_Volumes");
    EXPECT_STREQ(mesh.nodesGroupsNames[1].c_str(), "up_Nodes");
    EXPECT_STREQ(mesh.nodesGroupsNames[2].c_str(), "up1_Nodes");

    EXPECT_EQ(mesh.sdl.at(28-25), 1);
    EXPECT_EQ(mesh.sdl.at(27-25), 2);
    EXPECT_EQ(mesh.sdl.at(29-25), 2);
    EXPECT_EQ(mesh.sdl.at(26-25), 0);

    EXPECT_EQ(mesh.nodesGroups.at(8-1), 2);
    EXPECT_EQ(mesh.nodesGroups.at(2-1), 1);
    EXPECT_EQ(mesh.nodesGroups.at(4-1), 1);
    EXPECT_EQ(mesh.nodesGroups.at(3-1), 0);

}

TEST(loadUnvTest, loadUnvWithTransform) {
    std::string jsonSrc = testDataDir + "/one-cube-mesh.json";
    auto p = IoUtils::getParameters(jsonSrc);

    std::ifstream f(p.sourceFile);

    Mesh mesh1;
    bool loadRes = LoadUnv::go(p, &f, &mesh1, false);
    EXPECT_TRUE(loadRes);

    p.rUnv = {0, 11, 20};
    p.rReal = {0, 7, 30};
    p.zUnv = {0, 5, 10};
    p.zReal = {0, 8, 20};
    Mesh mesh2;

    f.close();
    f.open(p.sourceFile);
    loadRes = LoadUnv::go(p, &f, &mesh2, false);
    EXPECT_TRUE(loadRes);

    Interpolator iR(p.rUnv, p.rReal);
    Interpolator iZ(p.zUnv, p.zReal);

    std::function getR = [](const Eigen::RowVector3f& v) ->double  {return std::sqrt(v(0)*v(0)+v(1)*v(1));};

    for(auto i : {0, 2, 5, 19}) {// 1 3 6 20
        const Eigen::RowVector3f& n1 = mesh1.nodes.row(i)/p.kUnit;
        const Eigen::RowVector3f& n2 = mesh2.nodes.row(i)/p.kUnit;
        double r1 = getR(n1);
        double r2 = getR(n2);

        EXPECT_GE(2e-6, std::fabs(r2 - iR(r1))) << " for point " << n1 << ", r1 = " << r1 << " transformed to " << n2  << ", r2 = " << r2  << " expected r2 = " << iR(r1);
        EXPECT_GE(2e-6, std::fabs(n2(2) - iZ(n1(2)))) << " for point " << n1 << " transformed to " << n2  << " expected z2 = " << iZ(n1(2));
    }
}
TEST(loadUnvTest, STRESS_loadUnvTime) {

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    std::string jsonSrc = testDataDir + "/ms-mesh-yz-2t-sss-110k-coreAsCoil.json";
    auto p = IoUtils::getParameters(jsonSrc);

    std::ifstream f(p.sourceFile);

    Mesh mesh;
    bool loadRes = LoadUnv::go(p, &f, &mesh, false);
    EXPECT_TRUE(loadRes);

    end = std::chrono::system_clock::now();

    int milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>
            (end-start).count();
    std::cout << "elapsed milliseconds " << milliseconds << "\n";
}


TEST(storeUnvTest, test1) {
    DB db("");
    std::string jsonSrc = testDataDir + "/store-test-1.json";
    Parameters p;
    p.kUnit = 0.1;
    p.sourceFile = testDataDir + "/store-test-1.unv";
    p.resultFile = testDataDir + "/store-test-1.mat";

    Mesh mesh;
    mesh.nodes.resize(4, 3);
    mesh.nodes.row(0) = Eigen::RowVector3f {0, 0, 0.1};
    mesh.nodes.row(1) = Eigen::RowVector3f {1, 0, 0.1};
    mesh.nodes.row(2) = Eigen::RowVector3f {1, 0.5, 0.1};
    mesh.nodes.row(3) = Eigen::RowVector3f {0.2, 0.2, 1};

    mesh.tetrahedrons.resize(1, 4);
    mesh.tetrahedrons.row(0) = Eigen::RowVector4<Mesh::Index>{0, 1, 2, 3};

    bool storeRes = db.save(p, mesh, jsonSrc);
    EXPECT_TRUE(storeRes);
    Mesh mesh2;

    std::ifstream f(p.sourceFile);
    bool loadRes = LoadUnv::go(p, &f, &mesh2, false);
    EXPECT_TRUE(loadRes);
    ASSERT_EQ(mesh.nodes.rows(), mesh2.nodes.rows());
    ASSERT_EQ(mesh.tetrahedrons.rows(), mesh2.tetrahedrons.rows());
    for(size_t i = 0; i<mesh.nodes.rows(); i++)
        EXPECT_EQ(mesh.nodes.row(i), mesh2.nodes.row(i));
    for(size_t i = 0; i<mesh.tetrahedrons.rows(); i++)
        EXPECT_EQ(mesh.tetrahedrons.row(i), mesh2.tetrahedrons.row(i));
}

