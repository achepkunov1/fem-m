#include "gtest/gtest.h"

#include "common/Interpolator.h"
#include "common/NiceScale.h"


TEST(commonTest, NiceScale) {
    NiceScale scale(2.95, 4.05, 3);
    //std::cout << scale.lowerBound << ", " << scale.upperBound << ", " << scale.tickSpacing << ", " << scale.ticks << '\n';

    EXPECT_LT(std::abs(scale.lowerBound-2.95), scale.tickSpacing); // todo - improve to <= 0.5*scale.tickSpacing

    EXPECT_GE(scale.tickIndex(2.95), 1); // todo improve to = 0
    EXPECT_EQ(scale.tickIndex(-2.95), 0);

    for(int i = 0; i < scale.ticks; i++)
        EXPECT_EQ(scale.tickIndex(scale.tick(i)), i);
    // not worked because arg of scale.tick is unsigned:  EXPECT_EQ(scale.tickIndex(scale.tick(-1)), 0);
    EXPECT_EQ(scale.tickIndex(scale.tick(scale.ticks)), scale.ticks-1);

    EXPECT_EQ(scale.tickIndex(400), 3);
    EXPECT_LE(scale.tickIndex(4), 3);
}

TEST(commonTest, Interpolator) {
    //std::cout << testDataDir << "\n";
    Interpolator i({1, 2, 12}, {10, 0, 5});

    EXPECT_EQ(i(0), 20);
    EXPECT_EQ(i(1), 10);
    EXPECT_EQ(i(2), 0);
    EXPECT_EQ(i(12), 5);
    EXPECT_EQ(i(13), 5.5);

    EXPECT_DOUBLE_EQ(i(1.1), 9);
    EXPECT_DOUBLE_EQ(i(3), 0.5);

    i.multiplyTo(7);
    EXPECT_EQ(i(13), 5.5*7);
}
