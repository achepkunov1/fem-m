#include "gtest/gtest.h"

#include "Mesh.h"

TEST(meshTest, volume) {
    Mesh m;
    m.nodes.resize(4, Eigen::NoChange);
    m.nodes << 11, 10, 10,
               10, 10, 10,
               10, 11, 10,
               10, 10, 11;
    m.tetrahedrons.resize(1, Eigen::NoChange);
    m.tetrahedrons << 1, 0, 2, 3;
    double v = m.thVolume(0);
    EXPECT_DOUBLE_EQ(v, 1.0/6);
}

TEST(meshTest, bcc) {
    Mesh m;
    m.nodes.resize(4, Eigen::NoChange);
    m.nodes << 11, 10, 10,
            10, 10, 10,
            10, 11, 10,
            10, 10, 11;
    m.tetrahedrons.resize(1, Eigen::NoChange);
    m.tetrahedrons << 1, 0, 2, 3;
    Mesh::Bcc res( m.thBcc(0, Eigen::Vector3f(0, 0, 0)) );
    EXPECT_GT(res.maxCoeff(), 1.0);
    res = m.thBcc(0, m.nodes.row(0));
    EXPECT_EQ(res, Mesh::Bcc(0, 1, 0, 0));
    res = m.thBcc(0, m.nodes.row(3));
    EXPECT_EQ(res, Mesh::Bcc(0, 0, 0, 1));
    res = m.thBcc(0, m.nodes.colwise().sum()/4.0);
    EXPECT_EQ(res, Mesh::Bcc(0.25, 0.25, 0.25, 0.25));

}


TEST(meshTest, dlambdas) {
    Mesh m;
    m.nodes.resize(4, Eigen::NoChange);
    m.nodes << 10.1, 10, 10,
            10, 10, 10,
            10, 10.1, 10,
            10, 10, 10.1;
    m.tetrahedrons.resize(1, Eigen::NoChange);
    m.tetrahedrons << 1, 0, 2, 3;
    Eigen::Matrix<double, 3, 4> thNormals = m.thNormals(0);

    double d = double(m.nodes(0, 0))-10;
    EXPECT_EQ(thNormals.col(0), Eigen::Vector3d(-d*d, -d*d, -d*d));
    EXPECT_EQ(thNormals.col(1), Eigen::Vector3d(d*d, 0, 0));
    EXPECT_EQ(thNormals.col(2), Eigen::Vector3d(0, d*d, 0));
    EXPECT_EQ(thNormals.col(3), Eigen::Vector3d(0, 0, d*d));

}