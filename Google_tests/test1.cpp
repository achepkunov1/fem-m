#include "gtest/gtest.h"
#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"

#include "io/LoadUnv.h"
#include "solver/Solve.h"

TEST(Test1, io) {
    LoadUnv l;
    //EXPECT_FALSE(l.good());
}

TEST(Test1, solver) {
    Parameters param;
    Mesh mesh;
    Result res = Solver::go(param, mesh);
    EXPECT_FALSE(res.ok);
}

#pragma clang diagnostic pop