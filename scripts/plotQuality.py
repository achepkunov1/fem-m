import os

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import pandas as pd
import seaborn as sns
import matplotlib.cm as cm

import matplotlib.patches as patches

import json

import math

#import plotly.express as px

#fig = px.imshow([[1, 20, 30],
#                 [20, 1, 60],
#                 [30, 60, 1]])
#fig.show()

import numpy as np

import csv

        
        
if __name__ == '__main__':
    read_folder = os.getcwd()
    SRC = r'\..\test_data\mesh\ms-mesh-yz-2t-sss-110k-coreAsCoil-quality.csv'
    path = read_folder + SRC
    path = r'ms-mesh-Kar-quality2.csv'
    
    out_dir = 'data'
    if not os.path.isdir(out_dir):
        os.path.mkdir(out_dir)
        
    in_file = out_dir+r"\OlgaV2.json"
    in_file = out_dir+r"\FreeCAD-2tube-339k.json"
    #in_file = r'C:\projects\math\loadUnv\p360-2tubes-158k.json'
    in_file = r'C:\projects\math\loadUnv\p360-2tubes-158k.json'
    in_file = r'C:\projects\math\loadUnv\p360-2tubes-158k-nostr.json'
    #in_file = r'"M:\USERS\Adel Chepkunov\mesh\z\ms-mesh-yz-2t-sss-110k-coreAsCoil.json"'
    #in_file = out_dir+r"\Kar.json"
    out_file = out_dir+r"\out.csv"


    base_name, ext = os.path.splitext(in_file);
    
    command = r"%s\bin\quality-checker.exe -i %s -o %s -l -r 20 -c 21" % (read_folder, in_file, out_file)
    print (command)
    if os.system(command) != 0:
        print("error on collecting data by command")
        print(command)
        exit
    
    CSV = SRC
    CSV = 'data/out.csv'
    
    #my_data = np.genfromtxt(path, delimiter='\t')
    #my_data = np.genfromtxt(CSV, delimiter='\t')
    
    #print(my_data[1:,0])

    #xs = my_data[1:,0]
    #ys = my_data[1:,1]
    #zs = my_data[1:,2]
    
    #r'file:///' + read_folder + SRC
    my_data = pd.read_csv(out_file, delimiter='\t');
    
    my_data.head()
    
    xgrid = np.unique(xs)
    ygrid = np.unique(ys)
    

    title = in_file
    with open(in_file) as f:
        d = json.load(f)
        if "title" in d:
            title = d["title"]
    
    plt.figure(figsize=(20, 16))
    #ax = plt.scatter(pv_data)
    for column in ['nTau.avg', 'nTau.min', 'volume.min' ]:
        pv_data = my_data.pivot_table(values=column, index='z', columns='r')
        
        #pv_data.loc[df['z']=='ab', 'Price'] /= 1000
    
        plt.figure(figsize=(20, 16))
        heat_map = sns.heatmap(pv_data, annot=True, cmap='RdYlBu', vmax = 0.25)
        heat_map.set_yticklabels(heat_map.get_yticklabels(), rotation=35)
        plt.xlabel("Values of radiuses")
        plt.ylabel("Values of Z")
        plt.title("%s - %s" % (column, title), fontsize=16 )
        #ax=plt.gca()
        #rect = patches.Rectangle((0.125,0.100),10.50,10.25, edgecolor='r', facecolor="none")
        #ax.add_patch(rect)
    
        output_file = base_name+'_' + column + '.png'
        plt.savefig(output_file)
    

    my_data['z'] = map(my_data['z'])
    print()
    
    